#include <stddef.h>
struct slist { size_t length; size_t capacity; const char **strings; };
typedef struct slist slist;
#define SSET_BUCKETS 256
struct sset { slist buckets[SSET_BUCKETS]; };
typedef struct sset sset;

const char *sset_find(sset *set, const char *s);
const char *sset_cache(sset *set, const char *s);

