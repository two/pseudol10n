#include "config.h"
#include "set.h"
#include <locale.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
/* static const char **parse_line() { */

/* } */
/* struct rule {  }; */
static const char **get_table()
{
  const char *table_path = getenv("PSEUDOL10N_SUBST_TABLE");
  FILE *table_file = fopen(table_path, "r");
  if (!table_file) {
    perror("fopen \"$PSEUDOL10N_SUBST_TABLE\"");
    return NULL;
  }
  
  char *line = NULL;
  size_t buffer_size = 0;
  while (true) {
    ssize_t line_length = getline(&line, &buffer_size, table_file);
    if (line_length == -1) {
      perror("fread");
      break;
    }
    if (line_length == 0) {
      break;
    }
    // TODO: do something with the string
  }
  free(line);

  fclose(table_file);
}
/// return value = bytes to skip in the original string
static ssize_t find_sub(const char *head, const char **sub_ptr)
{
  if (*head == '%') {
    int skip = 1;
    while (head[skip] != ' ' && head[skip] != 0) {
      skip++;
    }
    *sub_ptr = NULL;
    return skip;
  }
  const char ***subs = cyr_table;
  while (*subs) {
    const char **sub = *subs;
    if (strncmp(sub[0], head, strlen(sub[0])) == 0) { // prefix
      *sub_ptr = sub[1];
      return strlen(sub[0]);
    }
    subs++;
  }
  *sub_ptr = NULL;
  return 1;
}
static size_t subst_len(const char *msg)
{
  size_t size = 0;
  const char *sub_ptr;
  while (*msg) {
    ssize_t skip_length = find_sub(msg, &sub_ptr);
    msg += skip_length;
    if (sub_ptr) {
      size += strlen(sub_ptr);
    } else {
      size += skip_length;
    }
  }
  return size;
}
void apply_subst(const char *orig, char *buffer)
{
  while (*orig) {
    const char *sub_ptr;
    ssize_t skip_length = find_sub(orig, &sub_ptr);
    if (!sub_ptr) {
      memcpy (buffer, orig, skip_length);
      buffer += skip_length;
    } else {
      strcpy(buffer, sub_ptr);
      buffer += strlen(sub_ptr);
    }
    orig += skip_length;
  }
  buffer[0] = 0;
}

static struct {
  char *buffer;
  size_t buffer_len;
  sset cache;
} globals = {};
static char *get_buffer(size_t size) {
  if (globals.buffer_len < size) {
    /* fprintf (stderr, "allocating to %d\n", (int) size); */
    globals.buffer = realloc(globals.buffer, size);
    globals.buffer_len = size;
  }
  return globals.buffer;
}

static const char *translation(const char *msgid) {
  /* fprintf(stderr, "translating: %s\n", msgid); */
  size_t result_len = subst_len(msgid) + 1;
  /* fprintf(stderr, "%zu -> %zu\n", strlen(msgid), result_len - 1); */
  char *buffer = get_buffer(result_len);
  apply_subst(msgid, buffer);
  /* fprintf(stderr, " => %s\n", buffer); */
  return sset_cache(&globals.cache, buffer);
}
const char *gettext(const char *msgid) {
  const char *context_end = strchr(msgid, 4); // ASCII EOT, marks end of msgctxt
  if (context_end) {
    msgid = context_end + 1;
  }
  return translation(msgid);
}
const char *dcgettext(const char *domainname, const char *msgid, int category) {
  if (category == __LC_MESSAGES) {
    return gettext(msgid);
  } else {
    return msgid;
  }
}
const char *glib_gettext(const char *msgid) {
  return gettext(msgid);
}
const char *g_dgettext(const char *domain, const char *msgid) {
  return gettext(msgid);
}

