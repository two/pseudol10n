escape() {
	local LC_ALL=C
	for ((i = 0; i < ${#1}; i++)); do
		printf '\\x%02x' "'${1:i:1}"
	done
}

echo "#include <stddef.h>"
counter=0
while read -a values; do
	echo -n "static const char *sub$counter[] = {"
	for value in "${values[@]}"; do
		echo -n "\"$(escape "$value")\", "
	done
	echo -n NULL
	echo "};"
	let counter=counter+1
done
echo -n "static const char **cyr_table[] = {"
printf "sub%d, " $(seq 0 $((counter-1)))
echo -n NULL
echo "};"