#include "set.h"
#include <assert.h>
#include <malloc.h>
#include <string.h>

// from http://www.cse.yorku.ca/~oz/hash.html
static unsigned long hash(const char *str)
{
  unsigned long hash = 5381;
  int c;

  while ((c = *str++))
      hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

  return hash % SSET_BUCKETS;
}
static const char *bucket_find(slist *list, const char *s) {
  for (size_t i = 0; i < list->length; i++) {
    const char *cs = list->strings[i];
    if (strcmp(s, cs) == 0) {
      return cs;
    }
  }
  return NULL;
}
static size_t new_capacity(size_t old) {
  return old == 0 ? 8 : old * 2;
}
static const char *bucket_cache(slist *list, const char *s) {
  const char *cs = bucket_find(list, s);
  if (cs) {
    return cs;
  }
  assert(list->capacity >= list->length);
  if (list->capacity == list->length) {
    size_t nc = new_capacity(list->capacity);
    /* fprintf(stderr, "%d\n", (int)nc); */
    list->strings = realloc(list->strings, nc * sizeof(const char*));
    list->capacity = nc;
  }
  const char *news = strdup(s);
  list->strings[list->length++] = news;
  return news;
}
const char *sset_find(sset *set, const char *s) {
  return bucket_find(&set->buckets[hash(s)], s);
}
const char *sset_cache(sset *set, const char *s) {
  return bucket_cache(&set->buckets[hash(s)], s);
}



