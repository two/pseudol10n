.PHONY: all
all: pseudol10n.so
config.h: cyr_table.txt table.sh
	bash table.sh <cyr_table.txt >config.h
CFLAGS = -Wall
pseudol10n.so: set.c set.h stubs.c config.h
	gcc $(CFLAGS) -shared -fPIC -o $@ set.c stubs.c

